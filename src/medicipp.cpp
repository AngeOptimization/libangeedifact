//
// Copyright Ange Optimization Aps, 2008.
// Author Esben Mose Hansen esben@ange.dk
//

#include "medicipp.h"
#include <xmltsg.h>
#include <istream>
#include <vector>
#include <cstring>
#include <stdexcept>
#include <sstream>
#include <iostream>
#include <QIODevice>
#include <QFile>

using std::string;

namespace ange {
namespace edifact {

static void errorhandler(void* obj, int error) {
  medicipp_t* m = static_cast<medicipp_t*>(obj);
  m->handle_error( error );
}

static void warninghandler(void* obj, int error) {
  medicipp_t* m = static_cast<medicipp_t*>(obj);
  m->handle_warning( error );
}

static void segmenthandler(void* obj, EDI_Parameters parameters, EDI_Segment segment, EDI_Directory directory) {
  medicipp_t* m = static_cast<medicipp_t*>(obj);
    m->handle_segment( parameters, segment, directory );
}

static void starthandler(void* obj, edi_event_t event, EDI_Parameters parameters) {
  medicipp_t* m = static_cast<medicipp_t*>(obj);
  m->handle_start( event, parameters );
}

#if 0
static void endhandler(void* obj, edi_event_t event) {
#else
static void endhandler(void* obj, edi_event_t event) {
#endif
  medicipp_t* m = static_cast<medicipp_t*>(obj);
  m->handle_end( event );
}

static EDI_Directory directoryhandler(void* obj, EDI_Parameters /* p */) {
  medicipp_t* m = static_cast<medicipp_t*>(obj);
  return m->directory();
}

medicipp_t::medicipp_t(const string& xmltsgfile): m_directory(0L), m_parser(0L) {
    static QByteArray data;
    // Probably never relevant, but in principle this section should be synchronized
    if (data.isEmpty()) {
        QFile xmltsg(xmltsgfile.c_str()); // allows expansion of QT compiled ressources
        xmltsg.open(QIODevice::ReadOnly);
        data = xmltsg.readAll();
    }
    m_directory = EDI_read_xmltsg_buffer(data.data(), data.size());
    initialize_parser();
}

medicipp_t::medicipp_t(char* buffer, unsigned size) {
  m_directory = EDI_read_xmltsg_buffer(buffer, size);
  initialize_parser();
}

void medicipp_t::initialize_parser() {
  // Create parser, register hooks.
  m_parser = EDI_ParserCreate();
  EDI_SetUserData( m_parser, this );
  EDI_SetWarningHandler( m_parser, &warninghandler );
  EDI_SetErrorHandler( m_parser, &errorhandler  );
  EDI_SetStartHandler( m_parser, &starthandler );
  EDI_SetEndHandler( m_parser, &endhandler );
  EDI_SetSegmentHandler( m_parser, &segmenthandler );
  EDI_SetPragma (m_parser, static_cast<EDI_Pragma>(EDI_PCHARSET | EDI_PSEGMENT | EDI_PTTC));

  // Set directory handler to return our directory parsed from xmltsg
  EDI_SetDirectoryHandler( m_parser, &directoryhandler );
}


medicipp_t::~medicipp_t() {

  // Work around double free error.
//   bool in_error =  (EDI_GetErrorCode(m_parser));

  if (m_parser) {
    EDI_ParserFree( m_parser );
  }
#if 0
  if (m_directory && !in_error) {
    EDI_DirectoryFree( m_directory );
  }
#endif
}

void medicipp_t::parse(QIODevice* stream) {
  char buffer[4096];
  int error = 0;
  while (!stream->atEnd()  && (error = EDI_GetErrorCode(m_parser)) == 0) {
    qint64 nbr_bytes_read = stream->read( &buffer[0], sizeof(buffer) );
    if (nbr_bytes_read<0) {
      throw std::runtime_error("Failed to read from stream: "+stream->errorString().toStdString());
    }
    EDI_Parse( m_parser, buffer, nbr_bytes_read, stream->atEnd() );
  }

  // if no exceptions emitted, but stream not parsed without errors to end 
  if (!stream->atEnd() || error != 0) {
      std::ostringstream os;
      os << "EDIFACT " << stream->errorString().toStdString()
         << " Stopped parsing at segment " << current_segment_index()
         << " and byte " << current_byte_index()
         << ": " << error << " " << error_string(error);
      throw std::runtime_error(os.str());
  }
}

const char* medicipp_t::error_string(int error) {
  return EDI_GetErrorString( error );
}

long medicipp_t::current_segment_index() {
  return EDI_GetCurrentSegmentIndex( m_parser );
}

long medicipp_t::current_byte_index() {
  return EDI_GetCurrentByteIndex( m_parser );
}

}
}
