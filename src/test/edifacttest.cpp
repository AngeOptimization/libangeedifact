#include <QTest>

#include "edifact_parser.h"

#include <ange/containers/oog.h>

#include <QFile>
#include <QObject>
#include <QList>

using namespace ange::units;
using ange::containers::Container;
using ange::containers::ContainerUndoBlob;
using ange::containers::EquipmentNumber;

class EdifactTest : public QObject {
    Q_OBJECT

public:

    class EdifactTestParser : public ange::edifact::edifact_parser_t {
    public:
        EdifactTestParser();
        ~EdifactTestParser();
        void openAndParse(const QString filename);
        QList<const ange::containers::Container *> & Containers();
        QList<const ange::edifact::edifact_parser_t::TankCondition *> & TankConditions();
    protected:
        void handle_container(const ange::containers::Container& container);
        void handleTankCondition(const TankCondition& tankCondition);
    private:
        QList<const ange::containers::Container *> mContainers;
        QList<const ange::edifact::edifact_parser_t::TankCondition *> mTankConditions;
    };

    virtual ~EdifactTest();

private Q_SLOTS:
    void initTestCase();
    void testBaplieTemplate();
    void testBaplieWrongSegmentOrder();
    void testBaplie95bAngeLong();
    void testBaplieInterchangeTrailerReferenceMismatch();
    // void testCoarri95bSeacos();
    void testCoarriTemplate();
    void testCoprarTemplate();
    // void testMovins95bPstow(); not working yet
    void testProjectionsTemplate();
    void testTansta93aPstow();
    void testTansta93aCargoas();
};

void EdifactTest::initTestCase(){
}

void EdifactTest::testBaplieTemplate() {
    EdifactTestParser parser;
    parser.openAndParse(QFINDTESTDATA("test-data/baplie-template.xls.edi"));

    QCOMPARE(parser.port(), QString("SDPZU"));
    QCOMPARE(parser.vessel_name(), QString("test88888888"));
    QCOMPARE(parser.voyage_code(), QString(""));
    QCOMPARE(parser.warnings().size(), 0);
    QCOMPARE(parser.Containers().size(), 13);

    QCOMPARE(parser.Containers()[0]->equipmentNumber(), EquipmentNumber("UACU3748328"));
    QCOMPARE(parser.Containers()[0]->loadPort(), QString("SDPZU"));
    QCOMPARE(parser.Containers()[0]->dischargePort(), QString("SAJED"));
    QCOMPARE(parser.Containers()[0]->isoCode(), ange::containers::IsoCode("22G0"));
    QCOMPARE(parser.Containers()[0]->weight(), 23920 * kilogram);
    QCOMPARE(parser.Containers()[0]->empty(), false);
    QCOMPARE(parser.Containers()[0]->live(), Container::NonLive);
    QVERIFY(parser.Containers()[0]->dangerousGoodsCodes().isEmpty());
    QCOMPARE(parser.Containers()[0]->handlingCodes().size(), 0);
    QCOMPARE(parser.Containers()[0]->carrierCode(), QString("UAS"));
    QVERIFY(parser.Containers()[0]->bookingNumber().isEmpty());
    QVERIFY(parser.Containers()[0]->placeOfDelivery().isEmpty());

    QCOMPARE(parser.Containers()[1]->equipmentNumber(), EquipmentNumber("UACU3450396"));
    QCOMPARE(parser.Containers()[1]->oog().top(), 10.0 * centimeter);
    QCOMPARE(parser.Containers()[1]->oog().front(), 0.0 * centimeter);
    QCOMPARE(parser.Containers()[1]->oog().back(), 0.0 * centimeter);
    QCOMPARE(parser.Containers()[1]->oog().left(), 7.0 * centimeter);
    QCOMPARE(parser.Containers()[1]->oog().right(), 5.0 * centimeter);

    QCOMPARE(parser.Containers()[2]->equipmentNumber(), EquipmentNumber("UACU5316302"));
    //QCOMPARE(parser.Containers()[2]->handlingCodes().size(), 4); see ticket #1051

    QCOMPARE(parser.Containers()[3]->equipmentNumber(), EquipmentNumber("UACU5541155"));
    QCOMPARE(parser.Containers()[3]->dangerousGoodsCodes().size(),2);
    QCOMPARE(parser.Containers()[3]->dangerousGoodsCodes().at(0).isLimitedQuantity(), false);
    QCOMPARE(parser.Containers()[3]->dangerousGoodsCodes().at(0).unNumber(), QString("0335"));
    QCOMPARE(parser.Containers()[3]->dangerousGoodsCodes().at(1).isLimitedQuantity(), true);
    QCOMPARE(parser.Containers()[3]->dangerousGoodsCodes().at(1).unNumber(), QString("3268"));

    QCOMPARE(parser.Containers()[11]->equipmentNumber(), EquipmentNumber("UACU4729240"));
    QCOMPARE(parser.Containers()[11]->isoCode(), ange::containers::IsoCode("45R0"));
    QCOMPARE(parser.Containers()[11]->live(), Container::Live);
    QCOMPARE(parser.Containers()[11]->temperature(), 5.0);

    QCOMPARE(parser.Containers()[12]->equipmentNumber(), EquipmentNumber("UACU4734041"));
    QCOMPARE(parser.Containers()[12]->loadPort(), QString("SAYBI"));
    QCOMPARE(parser.Containers()[12]->dischargePort(), QString("SAJED"));
    QCOMPARE(parser.Containers()[12]->isoCode(), ange::containers::IsoCode("45R0"));
    QCOMPARE(parser.Containers()[12]->weight(), 4200 * kilogram);
    QCOMPARE(parser.Containers()[12]->empty(), true);
    QCOMPARE(parser.Containers()[12]->live(), Container::NonLive);
    QVERIFY(parser.Containers()[12]->dangerousGoodsCodes().isEmpty());
    QCOMPARE(parser.Containers()[12]->handlingCodes().size(), 0);
    QCOMPARE(parser.Containers()[12]->carrierCode(), QString("IAL"));
    QVERIFY(parser.Containers()[12]->bookingNumber().isEmpty());
    QVERIFY(parser.Containers()[12]->placeOfDelivery().isEmpty());
}

void EdifactTest::testBaplieWrongSegmentOrder() {
    EdifactTestParser parser;
    parser.openAndParse(QFINDTESTDATA("test-data/baplie-1-wrong-order.edi"));

    QCOMPARE(parser.port(), QString("ESALG"));
    QCOMPARE(parser.vessel_name(), QString("HANSA LIMBURG"));
    QCOMPARE(parser.voyage_code(), QString("1330W"));
    QCOMPARE(parser.warnings().size(), 0);
    QCOMPARE(parser.Containers().size(), 3);

    // testing wrong order of segments: EQD-DGS-NAD
    QCOMPARE(parser.Containers()[0]->equipmentNumber(), EquipmentNumber("GESU6840369"));
    QVERIFY(parser.Containers()[0]->dangerousGoodsCodes().isEmpty());
    QCOMPARE(parser.Containers()[0]->carrierCode(), QString("HJS"));
    QCOMPARE(parser.Containers()[0]->placeOfDelivery(), QString("NGTIN"));

    QCOMPARE(parser.Containers()[1]->equipmentNumber(), EquipmentNumber("CRSU1404127"));
    QCOMPARE(parser.Containers()[1]->dangerousGoodsCodes().size(),1);
    QCOMPARE(parser.Containers()[1]->dangerousGoodsCodes().at(0).unNumber(),QString("1993"));
    QCOMPARE(parser.Containers()[1]->dangerousGoodsCodes().at(0).isLimitedQuantity(),false);
    QCOMPARE(parser.Containers()[1]->carrierCode(), QString("HJS"));

    QCOMPARE(parser.Containers()[2]->equipmentNumber(), EquipmentNumber("GLDU7587610"));
    QVERIFY(parser.Containers()[2]->dangerousGoodsCodes().isEmpty());
    QCOMPARE(parser.Containers()[2]->carrierCode(), QString("HJS"));
    QVERIFY(parser.Containers()[2]->placeOfDelivery().isEmpty());
}

void EdifactTest::testBaplie95bAngeLong() {
    EdifactTestParser parser;
    parser.openAndParse(QFINDTESTDATA("test-data/baplie-95b-ange-long.edi"));

    QCOMPARE(parser.port(), QString("SAYBI"));
    QCOMPARE(parser.vessel_name(), QString(""));
    QCOMPARE(parser.voyage_code(), QString(""));
    QCOMPARE(parser.warnings().size(), 0);
    QCOMPARE(parser.Containers().size(), 807);

    QCOMPARE(parser.Containers()[0]->equipmentNumber(), EquipmentNumber("UACU3826360"));
    QCOMPARE(parser.Containers()[0]->loadPort(), QString("SAYBI"));
    QCOMPARE(parser.Containers()[0]->dischargePort(), QString("SAJED"));
    QCOMPARE(parser.Containers()[0]->isoCode(), ange::containers::IsoCode("22G0"));
    QCOMPARE(parser.Containers()[0]->weight(), 19128 * kilogram);
    QCOMPARE(parser.Containers()[0]->empty(), false);
    QCOMPARE(parser.Containers()[0]->live(), Container::NonLive);
    QVERIFY(parser.Containers()[0]->dangerousGoodsCodes().isEmpty());
    QCOMPARE(parser.Containers()[0]->handlingCodes().size(), 0);
    QCOMPARE(parser.Containers()[0]->carrierCode(), QString("UAS"));
    QVERIFY(parser.Containers()[0]->bookingNumber().isEmpty());
    QCOMPARE(parser.Containers()[0]->placeOfDelivery(), QString("SAJED"));

    QCOMPARE(parser.Containers()[806]->equipmentNumber(), EquipmentNumber("BMSU2000310"));
    QCOMPARE(parser.Containers()[806]->loadPort(), QString("PZU"));
    QCOMPARE(parser.Containers()[806]->dischargePort(), QString("SAJED"));
    QCOMPARE(parser.Containers()[806]->isoCode(), ange::containers::IsoCode("22G0"));
    QCOMPARE(parser.Containers()[806]->weight(), 2200 * kilogram);
    QCOMPARE(parser.Containers()[806]->empty(), true);
    QCOMPARE(parser.Containers()[806]->live(), Container::NonLive);
    QVERIFY(parser.Containers()[806]->dangerousGoodsCodes().isEmpty());
    QCOMPARE(parser.Containers()[806]->handlingCodes().size(), 0);
    QCOMPARE(parser.Containers()[806]->carrierCode(), QString("BMC"));
    QVERIFY(parser.Containers()[806]->bookingNumber().isEmpty());
    QVERIFY(parser.Containers()[806]->placeOfDelivery().isEmpty());
}

void EdifactTest::testBaplieInterchangeTrailerReferenceMismatch() {
    EdifactTestParser parser;
    parser.openAndParse(QFINDTESTDATA("test-data/InterchangeTrailerReferenceMismatch.edi"));
    QCOMPARE(parser.Containers().size(), 2198);
}

// This test message has wrong order of segments DTM - expected was FTX, RFF. or  TDT
/*
void EdifactTest::testCoarri95bSeacos() {
    EdifactTestParser parser;
    parser.openAndParse(QFINDTESTDATA("test-data/coarri-95b-seacos-mac3.edi"));

    QCOMPARE(parser.port(), QString(""));
    QCOMPARE(parser.vessel_name(), QString(""));
    QCOMPARE(parser.voyage_code(), QString(""));
    QCOMPARE(parser.warnings().size(), 0);
    QCOMPARE(parser.Containers().size(), 13);

    QCOMPARE(parser.Containers()[0]->equipmentNumber(), EquipmentNumber(""));
    QCOMPARE(parser.Containers()[0]->loadPort(), QString(""));
    QCOMPARE(parser.Containers()[0]->dischargePort(), QString(""));
    QCOMPARE(parser.Containers()[0]->isocode(), ange::containers::IsoCode(""));
    QCOMPARE(parser.Containers()[0]->weight(),0 * kilogram);
    QCOMPARE(parser.Containers()[0]->empty(), false);
    QCOMPARE(parser.Containers()[0]->live(), Container::NonLive);
    QCOMPARE(parser.Containers()[0]->dg_codes_as_string(), QString(""));
    QCOMPARE(parser.Containers()[0]->handlingCodes().size(), 0);
    QVERIFY(parser.Containers()[0]->carrierCode().isEmpty());
    QVERIFY(parser.Containers()[0]->bookingNumber().isEmpty());
}
*/
// working on ticket #1029
void EdifactTest::testCoarriTemplate() {
    EdifactTestParser parser;
    parser.openAndParse(QFINDTESTDATA("test-data/coarri-template.xls.edi"));

    QCOMPARE(parser.port(), QString("CNTAG"));
    QCOMPARE(parser.vessel_name(), QString("test77777777"));
    QCOMPARE(parser.voyage_code(), QString("VES/1234"));
    QCOMPARE(parser.warnings().size(), 0);
    QCOMPARE(parser.Containers().size(), 25);

    QCOMPARE(parser.Containers()[0]->equipmentNumber(), EquipmentNumber("TGHU7498058"));
    QCOMPARE(parser.Containers()[0]->loadPort(), QString("CNTAG"));
    QCOMPARE(parser.Containers()[0]->dischargePort(), QString("USLAX"));
    QCOMPARE(parser.Containers()[0]->isoCode(), ange::containers::IsoCode("42P1"));
    QCOMPARE(parser.Containers()[0]->weight(), 22400 * kilogram);
    QCOMPARE(parser.Containers()[0]->empty(), false);
    QCOMPARE(parser.Containers()[0]->live(), Container::NonLive);
    QVERIFY(parser.Containers()[0]->dangerousGoodsCodes().isEmpty());
    QCOMPARE(parser.Containers()[0]->handlingCodes().size(), 0);
    QCOMPARE(parser.Containers()[0]->carrierCode(), QString(""));
    QCOMPARE(parser.Containers()[0]->bookingNumber(), QString("B0034"));
    QCOMPARE(parser.Containers()[0]->placeOfDelivery(), QString("USLAX"));

    QCOMPARE(parser.Containers()[4]->equipmentNumber(), EquipmentNumber("ZYXU8121768"));
    QCOMPARE(parser.Containers()[4]->oog().top(), 25.0 * centimeter);
    QCOMPARE(parser.Containers()[4]->oog().front(), 0.0 * centimeter);
    QCOMPARE(parser.Containers()[4]->oog().back(), 0.0 * centimeter);
    QCOMPARE(parser.Containers()[4]->oog().left(), 12.0 * centimeter);
    QCOMPARE(parser.Containers()[4]->oog().right(), 15.0 * centimeter);

    QCOMPARE(parser.Containers()[9]->equipmentNumber(), EquipmentNumber("GATU8580500"));
    QCOMPARE(parser.Containers()[9]->live(), Container::Live);
    QCOMPARE(parser.Containers()[9]->temperature(), 31.1111111111);

    QCOMPARE(parser.Containers()[12]->equipmentNumber(), EquipmentNumber("TGHU7928349"));
    QCOMPARE(parser.Containers()[12]->live(), Container::Live);
    QCOMPARE(parser.Containers()[12]->temperature(), 12.7);

    QCOMPARE(parser.Containers()[14]->equipmentNumber(), EquipmentNumber("BMOU2030330"));
    //QCOMPARE(parser.Containers()[14]->handlingCodes().size(), 3); see ticket #1051 , expected AB AL TS

    QCOMPARE(parser.Containers()[16]->equipmentNumber(), EquipmentNumber("CLHU8720539"));
    QCOMPARE(parser.Containers()[16]->dangerousGoodsCodes().size(), 2);
    QCOMPARE(parser.Containers()[16]->dangerousGoodsCodes().at(0).isLimitedQuantity(), false);
    QCOMPARE(parser.Containers()[16]->dangerousGoodsCodes().at(0).unNumber(), QLatin1String("0335"));
    QCOMPARE(parser.Containers()[16]->dangerousGoodsCodes().at(1).isLimitedQuantity(), true);
    QCOMPARE(parser.Containers()[16]->dangerousGoodsCodes().at(1).unNumber(), QLatin1String("3268"));

    QCOMPARE(parser.Containers()[24]->equipmentNumber(), EquipmentNumber("AMFU8551071"));
    QCOMPARE(parser.Containers()[24]->loadPort(), QString("CNTAG"));
    QCOMPARE(parser.Containers()[24]->dischargePort(), QString("USLAX"));
    QCOMPARE(parser.Containers()[24]->isoCode(), ange::containers::IsoCode("42T0"));
    QCOMPARE(parser.Containers()[24]->weight(), 29900 * kilogram);
    QCOMPARE(parser.Containers()[24]->empty(), false);
    QCOMPARE(parser.Containers()[24]->live(), Container::NonLive);
    QVERIFY(parser.Containers()[24]->dangerousGoodsCodes().isEmpty());
    QCOMPARE(parser.Containers()[24]->handlingCodes().size(), 0);
    QCOMPARE(parser.Containers()[24]->carrierCode(), QString(""));
    QCOMPARE(parser.Containers()[24]->bookingNumber(), QString("B0237"));
    QVERIFY(parser.Containers()[24]->placeOfDelivery().isEmpty());
}


void EdifactTest::testCoprarTemplate() {
    EdifactTestParser parser;
    parser.openAndParse(QFINDTESTDATA("test-data/coprar-template.xls.edi"));

    QCOMPARE(parser.port(), QString("CNTAG"));
    QCOMPARE(parser.vessel_name(), QString("test88888888"));
    QCOMPARE(parser.voyage_code(), QString("VES/1234"));
    QCOMPARE(parser.warnings().size(), 0);
    QCOMPARE(parser.Containers().size(), 25);

    QCOMPARE(parser.Containers()[0]->equipmentNumber(), EquipmentNumber("TGHU7498058"));
    QCOMPARE(parser.Containers()[0]->loadPort(), QString("CNTAG"));
    QCOMPARE(parser.Containers()[0]->dischargePort(), QString("USLAX"));
    QCOMPARE(parser.Containers()[0]->isoCode(), ange::containers::IsoCode("42P1"));
    QCOMPARE(parser.Containers()[0]->weight(), 22400 * kilogram);
    QCOMPARE(parser.Containers()[0]->empty(), false);
    QCOMPARE(parser.Containers()[0]->live(), Container::NonLive);
    QVERIFY(parser.Containers()[0]->dangerousGoodsCodes().isEmpty());
    QCOMPARE(parser.Containers()[0]->handlingCodes().size(), 0);
    QCOMPARE(parser.Containers()[0]->carrierCode(), QString(""));
    QCOMPARE(parser.Containers()[0]->bookingNumber(), QString("B0034"));
    QVERIFY(parser.Containers()[0]->placeOfDelivery().isEmpty());

    QCOMPARE(parser.Containers()[4]->equipmentNumber(), EquipmentNumber("ZYXU8121768"));
    QCOMPARE(parser.Containers()[4]->oog().top(), 25.0 * centimeter);
    QCOMPARE(parser.Containers()[4]->oog().front(), 0.0 * centimeter);
    QCOMPARE(parser.Containers()[4]->oog().back(), 0.0 * centimeter);
    QCOMPARE(parser.Containers()[4]->oog().left(), 12.0 * centimeter);
    QCOMPARE(parser.Containers()[4]->oog().right(), 15.0 * centimeter);

    QCOMPARE(parser.Containers()[9]->equipmentNumber(), EquipmentNumber("GATU8580500"));
    QCOMPARE(parser.Containers()[9]->live(), Container::Live);
    QCOMPARE(parser.Containers()[9]->temperature(), 31.1111111111);

    QCOMPARE(parser.Containers()[12]->equipmentNumber(), EquipmentNumber("TGHU7928349"));
    QCOMPARE(parser.Containers()[12]->live(), Container::Live);
    QCOMPARE(parser.Containers()[12]->temperature(), 12.7);

    QCOMPARE(parser.Containers()[14]->equipmentNumber(), EquipmentNumber("BMOU2030330"));
    //QCOMPARE(parser.Containers()[14]->handlingCodes().size(), 3); see ticket #1051 , expected AB AL TS

    QCOMPARE(parser.Containers()[16]->equipmentNumber(), EquipmentNumber("CLHU8720539"));
    QCOMPARE(parser.Containers()[16]->dangerousGoodsCodes().size(), 2);
    QCOMPARE(parser.Containers()[16]->dangerousGoodsCodes().at(0).isLimitedQuantity(), false);
    QCOMPARE(parser.Containers()[16]->dangerousGoodsCodes().at(0).unNumber(), QLatin1String("0335"));
    QCOMPARE(parser.Containers()[16]->dangerousGoodsCodes().at(1).isLimitedQuantity(), false);
    QCOMPARE(parser.Containers()[16]->dangerousGoodsCodes().at(1).unNumber(), QLatin1String("3268"));

    QCOMPARE(parser.Containers()[24]->equipmentNumber(), EquipmentNumber("AMFU8551071"));
    QCOMPARE(parser.Containers()[24]->loadPort(), QString("CNTAG"));
    QCOMPARE(parser.Containers()[24]->dischargePort(), QString("USLAX"));
    QCOMPARE(parser.Containers()[24]->isoCode(), ange::containers::IsoCode("42T0"));
    QCOMPARE(parser.Containers()[24]->weight(), 29900 * kilogram);
    QCOMPARE(parser.Containers()[24]->empty(), false);
    QCOMPARE(parser.Containers()[24]->live(), Container::NonLive);
    QVERIFY(parser.Containers()[24]->dangerousGoodsCodes().isEmpty());
    QCOMPARE(parser.Containers()[24]->handlingCodes().size(), 0);
    QCOMPARE(parser.Containers()[24]->carrierCode(), QString(""));
    QCOMPARE(parser.Containers()[24]->bookingNumber(), QString("B0237"));
    QVERIFY(parser.Containers()[24]->placeOfDelivery().isEmpty());
}

/*
void EdifactTest::testMovins95bPstow() {
    EdifactTestParser parser;
    parser.openAndParse(QFINDTESTDATA("test-data/movins-95b-pstow.edi"));

    QCOMPARE(parser.port(), QString(""));
    QCOMPARE(parser.vessel_name(), QString(""));
    QCOMPARE(parser.voyage_code(), QString(""));
    QCOMPARE(parser.warnings().size(), 0);
    QCOMPARE(parser.Containers().size(), 13);

    QCOMPARE(parser.Containers()[0]->equipmentNumber(), EquipmentNumber(""));
    QCOMPARE(parser.Containers()[0]->loadPort(), QString(""));
    QCOMPARE(parser.Containers()[0]->dischargePort(), QString(""));
    QCOMPARE(parser.Containers()[0]->isocode(), ange::containers::IsoCode(""));
    QCOMPARE(parser.Containers()[0]->weight(),0 * kilogram);
    QCOMPARE(parser.Containers()[0]->empty(), false);
    QCOMPARE(parser.Containers()[0]->live(), Container::NonLive);
    QCOMPARE(parser.Containers()[0]->dg_codes_as_string(), QString(""));
    QCOMPARE(parser.Containers()[0]->handlingCodes().size(), 0);
    QVERIFY(parser.Containers()[0]->carrierCode().isEmpty());
    QVERIFY(parser.Containers()[0]->bookingNumber().isEmpty());
    QVERIFY(parser.Containers()[0]->placeOfDelivery().isEmpty());
}
*/

void EdifactTest::testProjectionsTemplate() {
    EdifactTestParser parser;
    parser.openAndParse(QFINDTESTDATA("test-data/projections-template.xls.edi"));

    QCOMPARE(parser.port(), QString("CNTAG"));
    QCOMPARE(parser.vessel_name(), QString(""));
    QCOMPARE(parser.voyage_code(), QString("VES/1234"));
    QCOMPARE(parser.warnings().size(), 0);
    QCOMPARE(parser.Containers().size(), 134);

    QCOMPARE(parser.Containers()[0]->equipmentNumber(), EquipmentNumber("PROJ7652372"));
    QCOMPARE(parser.Containers()[0]->loadPort(), QString("CNTAG"));
    QCOMPARE(parser.Containers()[0]->dischargePort(), QString("USLAX"));
    QCOMPARE(parser.Containers()[0]->isoCode(), ange::containers::IsoCode("42P1"));
    QCOMPARE(parser.Containers()[0]->weight(), 26000 * kilogram);
    QCOMPARE(parser.Containers()[0]->empty(), false);
    QCOMPARE(parser.Containers()[0]->live(), Container::NonLive);
    QVERIFY(parser.Containers()[0]->dangerousGoodsCodes().isEmpty());
    QCOMPARE(parser.Containers()[0]->handlingCodes().size(), 0);
    QVERIFY(parser.Containers()[0]->carrierCode().isEmpty());
    QVERIFY(parser.Containers()[0]->bookingNumber().isEmpty());
    QVERIFY(parser.Containers()[0]->placeOfDelivery().isEmpty());

    QCOMPARE(parser.Containers()[108]->equipmentNumber(), EquipmentNumber("PROJ6088490"));
    QCOMPARE(parser.Containers()[108]->dangerousGoodsCodes().size(), 2);
    QCOMPARE(parser.Containers()[108]->dangerousGoodsCodes().at(0).isLimitedQuantity(), false);
    QCOMPARE(parser.Containers()[108]->dangerousGoodsCodes().at(0).unNumber(), QLatin1String("0335"));
    QCOMPARE(parser.Containers()[108]->dangerousGoodsCodes().at(1).isLimitedQuantity(), false);
    QCOMPARE(parser.Containers()[108]->dangerousGoodsCodes().at(1).unNumber(), QLatin1String("3268"));

    QCOMPARE(parser.Containers()[133]->equipmentNumber(), EquipmentNumber("PROJ1271484"));
    QCOMPARE(parser.Containers()[133]->loadPort(), QString("CNTAG"));
    QCOMPARE(parser.Containers()[133]->dischargePort(), QString("USLAX"));
    QCOMPARE(parser.Containers()[133]->isoCode(), ange::containers::IsoCode("22T0"));
    QCOMPARE(parser.Containers()[133]->weight(), 4000 * kilogram);
    QCOMPARE(parser.Containers()[133]->empty(), false);
    QCOMPARE(parser.Containers()[133]->live(), Container::NonLive);
    QVERIFY(parser.Containers()[133]->dangerousGoodsCodes().isEmpty());
    QCOMPARE(parser.Containers()[133]->handlingCodes().size(), 0);
    QVERIFY(parser.Containers()[133]->carrierCode().isEmpty());
    QVERIFY(parser.Containers()[133]->bookingNumber().isEmpty());
    QVERIFY(parser.Containers()[133]->placeOfDelivery().isEmpty());
}


void EdifactTest::testTansta93aPstow() {
    EdifactTestParser parser;
    parser.openAndParse(QFINDTESTDATA("test-data/TANSTAa4.edi"));

    QCOMPARE(parser.port(), QString("AEKLF"));
    QCOMPARE(parser.carrierCode(), QString("UAS"));
    QCOMPARE(parser.vessel_code(), QString("A6OG"));
    QCOMPARE(parser.vessel_name(), QString("ABU DHABI"));
    QCOMPARE(parser.voyage_code(), QString("1152W"));
    QCOMPARE(parser.warnings().size(), 0);
    QCOMPARE(parser.Containers().size(), 0);
    QCOMPARE(parser.TankConditions().size(), 56);

    QCOMPARE(parser.TankConditions()[0]->tankCode, QString("FORE PEAK C"));
    QCOMPARE(parser.TankConditions()[0]->tankName, QString("FORE PEAK C"));
    QCOMPARE(parser.TankConditions()[0]->weight/ton, 0.0);
    QCOMPARE(parser.TankConditions()[0]->density/ton_per_meter3, 1.025);

    QCOMPARE(parser.TankConditions()[15]->tankCode, QString("NO 3 SIDE S"));
    QCOMPARE(parser.TankConditions()[15]->tankName, QString("NO 3 SIDE S"));
    QCOMPARE(parser.TankConditions()[15]->weight/ton, 695.0);
    QCOMPARE(parser.TankConditions()[15]->density/ton_per_meter3, 1.025 );
}

void EdifactTest::testTansta93aCargoas() {
    EdifactTestParser parser;
    parser.openAndParse(QFINDTESTDATA("test-data/PUCON_DEP_ANR_TANSTA.edi"));

    QCOMPARE(parser.port(), QString(""));
    QCOMPARE(parser.carrierCode(), QString(""));
    QCOMPARE(parser.vessel_code(), QString("A8JW3"));
    QCOMPARE(parser.vessel_name(), QString("Pucon"));
    QCOMPARE(parser.voyage_code(), QString(""));
    QCOMPARE(parser.warnings().size(), 0);
    QCOMPARE(parser.Containers().size(), 0);
    QCOMPARE(parser.TankConditions().size(), 56);

    QCOMPARE(parser.TankConditions()[0]->tankCode, QString("TK001"));
    QCOMPARE(parser.TankConditions()[0]->tankName, QString("NO.1 D.B.W.B.T"));
    QCOMPARE(parser.TankConditions()[0]->weight/ton, 10.0);
    QVERIFY(qIsNaN(parser.TankConditions()[0]->density/ton_per_meter3));

    QCOMPARE(parser.TankConditions()[50]->tankCode, QString("TK051"));
    QCOMPARE(parser.TankConditions()[50]->tankName, QString("AIR COOLER DRA"));
    QCOMPARE(parser.TankConditions()[50]->weight/ton, 8.0);
    QVERIFY(qIsNaN(parser.TankConditions()[0]->density/ton_per_meter3));
}

EdifactTest::~EdifactTest(){
}


// class EdifactTest::EdifactTestParser

void EdifactTest::EdifactTestParser::openAndParse(const QString filename) {
    QFile edi(filename);
    QVERIFY(edi.open(QIODevice::ReadOnly));
    this->parse(&edi);
}

void EdifactTest::EdifactTestParser::handle_container(const Container& container) {
    ContainerUndoBlob* data = container.undoData();
    Container* cc = Container::createFromUndoData(data);
    Container::deleteUndoData(data);
    mContainers.append(cc);
}

void EdifactTest::EdifactTestParser::handleTankCondition(const TankCondition& tankCondition) {
    // qDebug() << "TANK:" << tankCondition.tankCode << tankCondition.tankName << tankCondition.weight/ton << tankCondition.density/ton_per_meter3;
    TankCondition* tc = new TankCondition(tankCondition); // deletion in ~EdifactTestParser()
    mTankConditions.append(tc);
}


EdifactTest::EdifactTestParser::EdifactTestParser() :  ange::edifact::edifact_parser_t() {
}

EdifactTest::EdifactTestParser::~EdifactTestParser() {
    if(!mContainers.isEmpty()) {
        qDeleteAll(mContainers);
        mContainers.clear();
    }
    if(!mTankConditions.isEmpty()) {
        qDeleteAll(mTankConditions);
        mTankConditions.clear();
    }
}

QList<const ange::containers::Container *> & EdifactTest::EdifactTestParser::Containers() {
    return mContainers;
}

QList<const ange::edifact::edifact_parser_t::TankCondition *> & EdifactTest::EdifactTestParser::TankConditions() {
    return mTankConditions;
}

QTEST_MAIN(EdifactTest);
#include "edifacttest.moc"
