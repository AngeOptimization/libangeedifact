#include "testreadinghandlingcodes.h"
#include <QtTest>
#include "../edifact_parser.h"
#include <QFile>

using namespace ange::units;

QTEST_MAIN(TestReadingHandlingCodes);

struct ExpectedData {
    ange::containers::EquipmentNumber equipmentNumber;
    QList<ange::containers::HandlingCode> handlingCodes;
    ange::units::Mass mass;
};



class Parser : public ange::edifact::edifact_parser_t {
    public:
        Parser() {
            constructExpectedDataSet();
        }
        virtual void handle_container(const ange::containers::Container& container) {
            QVERIFY(!m_expected.empty());
            ExpectedData expected = m_expected.takeFirst();
            QCOMPARE(container.equipmentNumber(), expected.equipmentNumber);
            Q_FOREACH(ange::containers::HandlingCode handling_code, container.handlingCodes()) {
                QCOMPARE(expected.handlingCodes.removeAll(handling_code), 1);
            }
            QVERIFY(expected.handlingCodes.isEmpty());
            QCOMPARE(expected.mass, container.weight());
        }
        virtual void addExpected(ExpectedData data) {
            m_expected << data;
        }
        virtual ~Parser() {
        }
    private:
        virtual void constructExpectedDataSet(){
            {
                ExpectedData data;
                data.equipmentNumber = ange::containers::EquipmentNumber("CLHU8322411");
                data.mass = 24200 * kilogram;
                data.handlingCodes << ange::containers::HandlingCode("DRY");
                addExpected(data);
            }
            {
                ExpectedData data;
                data.equipmentNumber = ange::containers::EquipmentNumber("BMOU2030330");
                data.mass = 22500 * kilogram;
                data.handlingCodes << ange::containers::HandlingCode("AB") << ange::containers::HandlingCode("AL") << ange::containers::HandlingCode("TS");
                addExpected(data);
            }
            {
                ExpectedData data;
                data.equipmentNumber = ange::containers::EquipmentNumber("ZYXU6275694");
                data.mass = 20900 * kilogram;
                data.handlingCodes << ange::containers::HandlingCode("AFH") << ange::containers::HandlingCode("INB");
                addExpected(data);
            }
        }

        QList<ExpectedData> m_expected;
};



void TestReadingHandlingCodes::readBaplieContainersWithHandlingCodes() {
    Parser parser;
    QFile f(QFINDTESTDATA("test-data/baplie-template-w-handlingcodes.xls.edi"));
    QVERIFY(f.exists());
    QVERIFY(f.open(QIODevice::ReadOnly));
    parser.parse(&f);
}

void TestReadingHandlingCodes::readCoarriContainersWithHandlingCodes() {
    Parser parser;
    QFile f(QFINDTESTDATA("test-data/coarri-template-w-handlingcodes.xls.edi"));
    QVERIFY(f.exists());
    QVERIFY(f.open(QIODevice::ReadOnly));
    parser.parse(&f);
}

void TestReadingHandlingCodes::readCoprarContainersWithHandlingCodes() {
    Parser parser;
    QFile f(QFINDTESTDATA("test-data/coprar-template-w-handlingcodes.xls.edi"));
    QVERIFY(f.exists());
    QVERIFY(f.open(QIODevice::ReadOnly));
    parser.parse(&f);
}





#include "testreadinghandlingcodes.moc"
