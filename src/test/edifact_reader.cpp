#include <iostream>
#include <QFile>

#include "../edifact_parser.h"

int main(int argc, char** argv) {
  if (argc!=2) {
      std::cerr << "Usage: " << argv[0] << "/path/to/edifact" << std::endl;
      std::cerr << std::endl;
      std::cerr << "Example: " << argv[0] << "baplie-template.xls.edi" << std::endl;
    exit(1);
  }

  QFile input(argv[1]);
  if (! input.open(QIODevice::ReadOnly)) {
      std::cerr << "Failed to open \"" << argv[1] << "\"\n";
      exit(1);
  }

  ange::edifact::edifact_parser_t parser;
  parser.parse( &input );
}
