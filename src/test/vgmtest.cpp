#include <QTest>
#include "edifact_parser.h"
#include <QFile>
#include <QObject>
#include <QList>

using namespace ange::units;
using ange::containers::Container;
using ange::containers::ContainerUndoBlob;

class VGMTest : public QObject {
    Q_OBJECT
public:
    class VGMTestParser : public ange::edifact::edifact_parser_t {
    public:
        VGMTestParser();
        ~VGMTestParser();
        void openAndParse(const QString filename);
        QList<const ange::containers::Container *> & Containers();
    protected:
        void handle_container(const ange::containers::Container& container);
    private:
        QList<const ange::containers::Container *> mContainers;
    };
private Q_SLOTS:
    void testBaplieTemplate();
};

void VGMTest::testBaplieTemplate() {
    VGMTestParser parser;
    parser.openAndParse(QFINDTESTDATA("test-data/baplie-vgm.edi"));
    QCOMPARE(parser.port(), QString("CNTAG"));
    QCOMPARE(parser.warnings().size(), 0);
    QCOMPARE(parser.Containers().size(), 3);
    QCOMPARE(parser.Containers()[0]->equipmentNumber(), ange::containers::EquipmentNumber("CLHU8322411"));
    QCOMPARE(parser.Containers()[0]->loadPort(), QString("CNTAG"));
    QCOMPARE(parser.Containers()[0]->dischargePort(), QString("USLAX"));
    QCOMPARE(parser.Containers()[0]->isoCode(), ange::containers::IsoCode("45R0"));
    QCOMPARE(parser.Containers()[0]->weight(), 24200 * kilogram);
    QCOMPARE(parser.Containers()[0]->weightIsVerified(), true);
    QCOMPARE(parser.Containers()[0]->empty(), false);
}

void VGMTest::VGMTestParser::openAndParse(const QString filename) {
    QFile edi(filename);
    QVERIFY(edi.open(QIODevice::ReadOnly));
    this->parse(&edi);
}

void VGMTest::VGMTestParser::handle_container(const Container& container) {
    ContainerUndoBlob* data = container.undoData();
    Container* cc = Container::createFromUndoData(data);
    Container::deleteUndoData(data);
    mContainers.append(cc);
}

VGMTest::VGMTestParser::VGMTestParser() : ange::edifact::edifact_parser_t() {
}

VGMTest::VGMTestParser::~VGMTestParser() {
    if(!mContainers.isEmpty()) {
        qDeleteAll(mContainers);
        mContainers.clear();
    }
}

QList<const ange::containers::Container *> & VGMTest::VGMTestParser::Containers() {
    return mContainers;
}

QTEST_MAIN(VGMTest);
#include "vgmtest.moc"
