#ifndef TESTREADINGHANDLINGCODES_H
#define TESTREADINGHANDLINGCODES_H

#include <QObject>

class TestReadingHandlingCodes : public QObject {
    Q_OBJECT
    private Q_SLOTS:
        void readBaplieContainersWithHandlingCodes();
        void readCoarriContainersWithHandlingCodes();
        void readCoprarContainersWithHandlingCodes();
};

#endif // TESTREADINGHANDLINGCODES_H
