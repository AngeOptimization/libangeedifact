#include "edifact_parser.h"

#include <iostream>
#include <cstring>
#include <stdexcept>
#include <cassert>
#include <sstream>
#include <medici.h>

#include <QStringList>
#include <QDateTime>
#include <ange/containers/dangerousgoodscode.h>
#include <ange/containers/handlingcode.h>
#include <ange/containers/oog.h>
#include <ange/units/units.h>

using namespace ange::units;
using ange::containers::Container;

using std::cout;
using std::endl;
using std::cerr;
using std::string;
using std::ostringstream;

// change this to get more noisy debug output for tests and stuff
#define NOISY_DEBUG 0

namespace ange {
namespace edifact {

template<typename S> S lexical_cast(const char* in) {
  std::istringstream iss(in);
  S rv;
  iss >> rv;
  if (!iss) {
    throw std::runtime_error("Failed case from '" + std::string(in));
  }
  return rv;
};

struct edifact_parser_private_t : public QSharedData {
  edifact_parser_private_t();

  struct Container {
    Container();
    double weight;
    bool weightIsVerified;
    QString load_uncode;
    QString discharge_uncode;
    QString transshipment_uncode;
    QString raw_position;
    int bay;
    int row;
    int tier;
    bool reefer;
    double reefer_temperature; // always Celsius
    bool empty;
    QString equipmentnumber;
    ange::containers::IsoCode isocode;
    QList<ange::containers::DangerousGoodsCode> dangerous_goods_codes;
    QList<ange::containers::HandlingCode> handling_codes;
    ange::containers::Oog oog;
    QString booking_number;
    QString carrier_code;
    QString placeOfDelivery;
  } container;

    edifact_parser_t::TankCondition tankCondition;

  struct metadata_t {
    metadata_t();
    QDateTime message_time;
    QString message_ref;
    QString vessel_name;
    QString voyage_code;
    QString vessel_code;
    QDateTime vessel_arrival;
    QString carrier_code;
    QString port_uncode;
    QString load_uncode;
    QString discharge_uncode;
    QString message_version;
    QString message_release;
    edifact_parser_t::doc_type_t doc_type;
    edifact_parser_t::direction_t direction;
  } metadata;

  QList<int> codes;
  QStringList warnings;
};

edifact_parser_private_t::Container::Container() : reefer(false), empty(false), isocode("42ZZ") {}

edifact_parser_private_t::metadata_t::metadata_t() :
    doc_type(edifact_parser_t::UNKNOWN_DOC_TYPE),
    direction(edifact_parser_t::UNKNOWN_DIRECTION)
{}

edifact_parser_private_t::edifact_parser_private_t() :
    metadata()
{}

edifact_parser_t::edifact_parser_t() :
medicipp_t(":/libangeedifact/data/d95b_sloppy_d93a_tansta.xml"), d(new edifact_parser_private_t)
{}

edifact_parser_t::~edifact_parser_t() {}

void edifact_parser_t::parse(QIODevice* input) {
  medicipp_t::parse( input );
}

void edifact_parser_t::handle_warning(int error) {
    QString message = QString("EDIFACT warning at segment %1 and byte %2: %3 %4").arg(current_segment_index()).arg(current_byte_index()).arg(error).arg(error_string(error));
    d->warnings << message;
    qWarning() << message;
    throw std::runtime_error(message.toStdString()); // essential for error handling in stow 'bool edifact_container_reader_t::parse(QString filename)'
}

void edifact_parser_t::handle_error(int error) {
    QString message = QString("EDIFACT error at segment %1 and byte %2: %3 %4")
        .arg(current_segment_index()).arg(current_byte_index()).arg(error).arg(error_string(error));
    d->warnings << message;

    /**
     * Ignoring EDI_ETTR (Transaction trailer reference mismatch) and
     *          EDI_EITR (Interchange trailer reference mismatch)
     * as these errors commonly occur in real-life baplies and failing the edifact import
     * on them is too restrictive.
     */
    if (error == EDI_ETTR || error == EDI_EITR) {
        return;
    }

    /**
     * Ignoring EDI_ETTC (Transaction trailer count incorrect) and
     *          EDI_EITC (Interchange trailer count incorrect)
     * as these errors commonly occurs when we test and failing the edifact import
     * on them is too restrictive.
     */
    if (error == EDI_ETTC || error == EDI_EITC) {
        return;
    }

    // Essential for error handling in stow 'bool edifact_container_reader_t::parse(QString filename)'
    throw std::runtime_error(message.toStdString());
}

void edifact_parser_t::add_warning(const QString& text) {
  ostringstream os;
  os << "Warning at segment " << current_segment_index() << " and byte "
  << current_byte_index() << ": " << text.toStdString();
  d->warnings << QString::fromStdString(os.str());
  cerr << os.str() << endl;
}

void edifact_parser_t::handle_baplie_segment(EDI_Parameters /*parameters*/, EDI_Segment segment, EDI_Directory /*directory*/) {
    if ( d->codes.empty()) {
        string code(EDI_GetCode(segment));
        if ( code == "DTM") {
            handle_dtm(segment, true, false);
        } else if (code == "UNH"){
            if (char* value = EDI_GetElement(segment,0,0)) {
                QString ref = QString::fromUtf8(value);
                d->metadata.message_ref=ref;
                handle_doc_meta(MESSAGE_REF,ref);
            }
        }
    } else if (d->codes.back()==1) { // grp1:  TDT - LOC - DTM - RFF - FTX
        handle_tdt_baplie( segment, true );
        handle_dtm(segment, true, true);
        handle_loc_metadata(segment);
    } else if (d->codes.back()==2 ) { // grp2: LOC - GID - GDS - FTX - MEA - DIM - TMP - RNG - LOC - RFF - grp3 - grp4
        handle_loc_container_baplie_coarri(segment);
        handle_ftx_han(segment);
        handle_mea(segment);
        handle_tmp(segment);
        handle_dim(segment);
    } else if (d->codes.back()==3) { // grp3: EQD - EQA - NAD
        handle_eqd(segment);
        handle_nad(segment);
    } else if (d->codes.back()==4) { // grp4: DGS - FTX
        handle_dgs(segment);
        handle_nad(segment); // sloppy parsing of wrong segment order EQD - DGS - NAD, ticket #1043
    }
}


void edifact_parser_t::handle_coarri_segment(EDI_Parameters /*parameters*/, EDI_Segment segment, EDI_Directory /*directory*/) {
    string code(EDI_GetCode(segment));
    if ( d->codes.empty()) {
        string code(EDI_GetCode(segment));
        if (code == "UNH"){
            if (char* value = EDI_GetElement(segment,0,0)) {
                QString ref = QString::fromUtf8(value);
                d->metadata.message_ref=ref;
                handle_doc_meta(MESSAGE_REF,ref);
            }
        }
    } else if (d->codes.back()==1) { // group 1: TDT-RFF-LOC-DTM
        handle_tdt_baplie(segment, true);
        handle_loc_metadata_coprar(segment);
        handle_dtm(segment, true, true);
    } else if (d->codes.back()==2)  { // group 2 ignored: NAD-CTA
    } else if (d->codes.back()==3 ) { // group 3: EQD-RFF-TMD-DTM-LOC-MEA-DIM-TMP-RNG-SEL-FTX-DGS-EQA-PIA-SG4-SG5-NAD
        handle_eqd(segment);
        handle_rff(segment);
        handle_loc_container_baplie_coarri(segment);
        handle_mea(segment);
        handle_dim(segment);
        handle_tmp(segment);
        handle_ftx_han(segment);
        handle_dgs(segment);
        handle_nad(segment);
    } else if (d->codes.back()==4) { // group 4 ignored: DAM-COD
    } else if (d->codes.back()==5) { // group 5 ignored: TDT-LOC-DTM
    }
}


void edifact_parser_t::handle_coprar_segment(EDI_Parameters /*parameters*/, EDI_Segment segment, EDI_Directory /*directory*/) {
    string code(EDI_GetCode(segment));
    if ( d->codes.empty()) {
        string code(EDI_GetCode(segment));
        if (code == "UNH"){
            if (char* value = EDI_GetElement(segment,0,0)) {
                QString ref = QString::fromUtf8(value);
                d->metadata.message_ref=ref;
                handle_doc_meta(MESSAGE_REF,ref);
            }
        }
    } else if (d->codes.back()==1) {
        handle_tdt_baplie( segment, true );
        handle_loc_metadata_coprar(segment);
        handle_dtm(segment, true, true);
    } else if (d->codes.back()==3 ) {
        handle_loc_container_coprar(segment);
        handle_mea(segment);
        handle_dim(segment);
        handle_dgs(segment);
        handle_tmp(segment);
        handle_rff(segment);
        handle_nad(segment);
        handle_eqd(segment);
        handle_ftx_han(segment);
    } else if (d->codes.back()==4) {
        handle_tdt(segment, false);
    }
}


void edifact_parser_t::handle_movins_segment(EDI_Parameters /*parameters*/, EDI_Segment segment, EDI_Directory /*directory*/)
{
    if ( d->codes.empty()) {
        string code(EDI_GetCode(segment));
        if ( code == "DTM") {
            handle_dtm(segment, true, false);
        } else if (code == "UNH"){
            if (char* value = EDI_GetElement(segment,0,0)) {
                QString ref = QString::fromUtf8(value);
                d->metadata.message_ref=ref;
                handle_doc_meta(MESSAGE_REF,ref);
            }
        }
    } else if (d->codes.back()==1) {
        handle_tdt_movins( segment, true );
        handle_loc_metadata(segment);
        handle_dtm(segment, true, true);
    } else if (d->codes.back()==2 ) {
        handle_han_direction(segment);
    } else if (d->codes.back()==3 ) {
        handle_loc_container_movins(segment);
        handle_mea(segment);
        handle_tmp(segment);
    } else if (d->codes.back()==4) {
        handle_eqd(segment);
    }
}


void edifact_parser_t::handleTanstaSegment(EDI_Parameters /*parameters*/, EDI_Segment segment, EDI_Directory /*directory*/) {
    // coded using TANSTA:S:93A:UN:SMDG03 reference
    string code(EDI_GetCode(segment));
    if ( d->codes.empty()) {
        string code(EDI_GetCode(segment));
        if (code == "UNH"){
            if (char* value = EDI_GetElement(segment,0,0)) {
                QString ref = QString::fromUtf8(value);
                d->metadata.message_ref=ref;
                handle_doc_meta(MESSAGE_REF,ref);
            }
        }
    }
    // Note: have edited EDIFACT TANSTA d96a xml stanza to be conform with TANSTA:S:93A:UN:SMDG03 reference, such that EDIFACT groups are matching
    else if (d->codes.back()==1) { // Group grp1 : TDT - LOC - DTM - RFF - FTX
        handleTdtTansta(segment);
        handleLocMetadataTansta(segment);
        handle_dtm(segment, false, true);
        // ignore RFF
        // ignore FTX
    } else if (d->codes.back()==2 ) { // Group grp2 : LOC - MEA - DIM
        handleLocTansta(segment);
        handleMeaTansta(segment);
        // ignore DIM
    }
}


void edifact_parser_t::handle_dgs(EDI_Segment segment) {
    if (string(EDI_GetCode(segment)) != "DGS") { return; }
    if (EDI_GetElementCount(segment) < 3) { return; }

    QString undg_number = QString::fromUtf8(EDI_GetElement(segment, 2,0));
    d->container.dangerous_goods_codes << ange::containers::DangerousGoodsCode(undg_number);
}


void edifact_parser_t::handle_dim(EDI_Segment segment) {
    if (string(EDI_GetCode(segment)) != "DIM") { return; }
    if (EDI_GetElementCount(segment) < 2) { return; }

    if (char* value = EDI_GetElement(segment,0,0)) {
      const int category = lexical_cast<int>(value);
      if(char* unit_value = EDI_GetElement(segment,1,0)) {
        string Oogype = unit_value;
        if (Oogype == "CMT" || Oogype == "CM" || Oogype == "INH") {
          char* oog_value = 0;
          if(category == 5 || category == 6) {
            oog_value = EDI_GetElement(segment,1,1);
          } else if (category == 7 || category == 8) {
            oog_value = EDI_GetElement(segment,1,2);
          } else if (category == 9 /*movins & baplie & coprar 1.2*/ || category == 13 /*coprar 2.0*/) {
            oog_value = EDI_GetElement(segment,1,3);
          }
          if(!oog_value) {
            return;
          }
          double oog_measure = lexical_cast<double>(oog_value); // OOG size in centimeter
          if(Oogype=="INH") {
            oog_measure *= 2.54;
          }
          switch(category) {
            case 1:
              //gross dimension (only for break bulk) TODO: all three dimensions will be in same segment!
              break;
            case 5:
              d->container.oog.setFront(oog_measure * centimeter);
              break;
            case 6:
              d->container.oog.setBack(oog_measure * centimeter);
              break;
            case 7:
              d->container.oog.setRight(oog_measure * centimeter);
              break;
            case 8:
              d->container.oog.setLeft(oog_measure * centimeter);
              break;
            case 9:  //baplie & movins & coprar 1.2
              d->container.oog.setTop(oog_measure * centimeter);
              break;
            case 10:
              // external dimensions (Non-ISO only) TODO: all three dimensions will be in same segment!
              break;
            case 13: //coprar 2.0
              d->container.oog.setTop(oog_measure * centimeter);
              break;
            default:
              //nothing
              ;;
          }
        }
      }
    }
}


void edifact_parser_t::handle_dtm(EDI_Segment segment, bool global, bool vessel) {
    if (string(EDI_GetCode(segment)) != "DTM") { return; }

    int type_code = 137;
    if(d->metadata.doc_type == BAPLIE && vessel){
        type_code = 178;
    } else if (d->metadata.doc_type == MOVINS && vessel){
        type_code = 132;
    } else if (d->metadata.doc_type == COPRAR && vessel){
        type_code = 132;
    }
    if (lexical_cast<int>(EDI_GetElement( segment,0,0 ))==type_code) {
        QDateTime datetime;
        QString format;
        if (char* value = EDI_GetElement(segment,0,2)) {
            int type = lexical_cast<int>(value);
            if ( type == 101) {
                format = "yyMMdd";
            } else if ( type == 201) {
                format = "yyMMddHHmm";
            } else if ( type == 301 ) {
                format = "yyMMddHHmm";
            } else if ( type == 203) {
                format = "yyyyMMddHHmm";
            }
        }
        if (char* value = EDI_GetElement(segment,0,1)) {
            datetime = QDateTime().fromString(QString::fromUtf8(value),format);
            if(datetime.date().year() < 1950 ){
                datetime = datetime.addYears(100);
            }
        }
        if (global) {
            if (vessel) {
                d->metadata.vessel_arrival = datetime;
                handle_doc_meta( VESSEL_ARRIVAL_TIME, d->metadata.vessel_arrival.toString(Qt::ISODate));
            } else {
                d->metadata.message_time = datetime;
                handle_doc_meta( MESSAGE_TIME, d->metadata.message_time.toString(Qt::ISODate));
            }
        }
    }
}


void edifact_parser_t::handle_eqd(EDI_Segment segment) {
    if (string(EDI_GetCode(segment)) != "EQD") { return; }
    if (EDI_GetElementCount(segment) < 2) { return; }

    string type;
    if (char* value = EDI_GetElement(segment,0,0)) {
        type = value;
    }
    if (type=="CN") {
        if (char* value = EDI_GetElement(segment,1,0)) {
            d->container.equipmentnumber = QString::fromUtf8(value);
        }
        if (char* value = EDI_GetElement(segment,2,0)) {
            d->container.isocode = ange::containers::IsoCode(QString::fromUtf8(value));
        }
        if (char* value = EDI_GetElement(segment,5,0)) {
            d->container.empty = (QString::fromUtf8(value) == "4");  // default: 5 ~ full
        }
    }
}


void edifact_parser_t::handle_ftx_han(EDI_Segment segment){
    if (string(EDI_GetCode(segment)) != "FTX") {
        return;
    }

    // only allow one single FTX+HAN segment
    // FTX+HAN+++AFH'
    char* value = EDI_GetElement(segment,0,0);
    string text_subject_qualifier = value;
    if (text_subject_qualifier != "HAN") {
        return;
    }
    value = EDI_GetElement(segment,3,0);
    if (!value) {
        return;
    }
    d->container.handling_codes << ange::containers::HandlingCode(QString::fromUtf8(value));
}


void edifact_parser_t::handle_han_direction(EDI_Segment segment) {
    if(string(EDI_GetCode(segment)) != "HAN"){ return; }

    if (char* value = EDI_GetElement(segment,0,0)) {
        QString sw = QString::fromUtf8(value);
        direction_t direction = UNKNOWN_DIRECTION;
        if ( sw == "LOA" ) {
            direction = LOAD;
        } else if ( sw == "DIS" ) {
            direction = DISCHARGE;
        } else if ( sw == "RES" ) {
            direction = RESTOW;
        }
        Q_ASSERT(direction != UNKNOWN_DIRECTION);
        handle_section(DIRECTION_SECTION_TYPE,static_cast<int>(direction));
    }
}


void edifact_parser_t::handle_loc_container_baplie_coarri(EDI_Segment segment){
    if (string(EDI_GetCode(segment)) != "LOC") { return; }
    if (EDI_GetElementCount( segment ) < 2) { return; }

    // Position, load and discharge ports (also other data)
    int type = -1;
    if (char* value = EDI_GetElement(segment,0,0)) {
        type = lexical_cast<int>(value);
    }
    string data;
    if (char* value = EDI_GetElement(segment,1,0)) {
        switch (type) {
            case 6: // Baplie 1.5 load port
                d->container.load_uncode = QString::fromUtf8(value);
                break;
            case 7: // Coarri 1.2 place of delivery
                d->container.placeOfDelivery = QString::fromUtf8(value);
                break;
            case 9: // Baplie 2.1 + Coarri 1.2 + 2.0 load port
                d->container.load_uncode = QString::fromUtf8(value);
                break;
            case 11: // Baplie 2.1 + Coarri 2.0 disch port
                d->container.discharge_uncode = QString::fromUtf8(value);
                break;
            case 12: // Baplie 1.5 disch port
                d->container.discharge_uncode = QString::fromUtf8(value);
                break;
            case 83: // Baplie 1.5 + Baplie 2.1 Place of delivery
                d->container.placeOfDelivery = QString::fromUtf8(value);
                break;
            case 152: // Baplie 2.1 Next port of discharge
                //           m_container.next_port(data);
                break;
            case 147:{
                QString rawposition = QString::fromUtf8(value);
                d->container.raw_position = rawposition;
                if (char* t = EDI_GetElement(segment,1,2)) {
                    QString tt = QString::fromUtf8(t);
                    if((rawposition.size() == 7)
                        && (tt == "5" || tt == "87" || tt == "ZZZ")) {
                        // accept ISO Bay/Row/Tier (BBBRRTT) content in Feeder format #1476
                        d->container.bay = QString(rawposition).remove(3,4).toInt();
                        d->container.row = QString(rawposition).remove(5,2).remove(0,3).toInt();
                        d->container.tier = QString(rawposition).remove(0,5).toInt();
                    } else {
                        add_warning(QString("Expected ISO-format: 5 Bay/Row/Tier (BBBRRTT), got format: ")
                                    + tt + " content: " + rawposition);
                    }
                }
                break;
            }
            default:
                // Ignore
                break;
        }
    }
}


void edifact_parser_t::handle_loc_container_coprar(EDI_Segment segment){
    // Position, load and discharge ports (also other data)
    if (string(EDI_GetCode(segment)) != "LOC") { return; }
    if (EDI_GetElementCount( segment ) < 2) { return; }

    int type = -1;
    if (char* value = EDI_GetElement(segment,0,0)) {
        type = lexical_cast<int>(value);
    }
    if (char* value = EDI_GetElement(segment,1,0)) {
        switch (type) {
            case 7:
                d->container.placeOfDelivery = QString::fromUtf8(value);
                break;
            case 9: // Load port
                d->container.load_uncode = QString::fromUtf8(value);
                break;
            case 11:
                d->container.discharge_uncode = QString::fromUtf8(value);
                break;
            case 147:
                d->container.raw_position = QString::fromUtf8(value);
                break;
            default:
                // Ignore
                break;
        }
    }
}


void edifact_parser_t::handle_loc_container_movins(EDI_Segment segment){
    if (string(EDI_GetCode(segment)) != "LOC") { return; }
    if (EDI_GetElementCount(segment) < 2) { return; }

    int type = -1;
    if (char* value = EDI_GetElement(segment,0,0)) {
        type = lexical_cast<int>(value);
    }
    string data;
    if (char* value = EDI_GetElement(segment,1,0)) {
        switch (type) {
            case 6: // Alt load port
                if (d->container.load_uncode.isEmpty()) {
                    d->container.load_uncode = QString::fromUtf8(value);
                }
                break;
            case 9: // Load port
                d->container.load_uncode = QString::fromUtf8(value);
                break;
            case 11:
                d->container.discharge_uncode = QString::fromUtf8(value);
                break;
            case 13:
                if (d->container.transshipment_uncode.isEmpty()) {
                    d->container.transshipment_uncode = QString::fromUtf8(value);
                }
                break;
            case 83:
                d->container.placeOfDelivery = QString::fromUtf8(value);
                break;
            case 152:
                //           m_container.next_port(data);
                break;
            case 147:{
                QString rawposition = QString::fromUtf8(value);
                d->container.raw_position = rawposition;
                if (char* t = EDI_GetElement(segment,1,2)) {
                    QString tt = QString::fromUtf8(t);
                    if(tt == "5") {
                        d->container.bay = QString(rawposition).remove(3,4).toInt();
                        d->container.row = QString(rawposition).remove(5,2).remove(0,3).toInt();
                        d->container.tier = QString(rawposition).remove(0,5).toInt();
                    } else if (tt == "87"){
                        d->container.bay = QString(rawposition).remove(3,4).toInt();
                        d->container.tier = QString(rawposition).remove(5,2).remove(0,3).toInt();
                        d->container.row = QString(rawposition).remove(0,5).toInt();
                    } else if (tt == "ZZZ"){
                        add_warning("Unsupported bay,row,tier format");
                    }
                }
                break;
            }
            default:
                // Ignore
                break;
        }
    }
}

void edifact_parser_t::handle_loc_metadata(EDI_Segment segment){
    if (string(EDI_GetCode(segment)) != "LOC") { return; }

    if (char* value = EDI_GetElement(segment,0,0)) {
        int location_function = lexical_cast<int>(value);
        if (char* value = EDI_GetElement(segment, 1, 0)) {
            if (location_function==5) {
                d->metadata.port_uncode = QString::fromUtf8(value);
            }
        }
    }
}


void edifact_parser_t::handle_loc_metadata_coprar(EDI_Segment segment){
    if (string(EDI_GetCode(segment)) != "LOC") { return; }

    if (char* value = EDI_GetElement(segment,0,0)) {
        int location_function = lexical_cast<int>(value);
        d->metadata.direction = (location_function == 9) ? LOAD : DISCHARGE;
        if (char* value = EDI_GetElement(segment, 1, 0)) {
            if (location_function==9) {
                d->metadata.load_uncode = QString::fromUtf8(value);
            } else if (location_function==11) {
                d->metadata.discharge_uncode = QString::fromUtf8(value);
            }
        }
    }
}


void edifact_parser_t::handleLocMetadataTansta(EDI_Segment segment) {
    if (string(EDI_GetCode(segment)) != "LOC") { return; }

    QString type = "";
    if (char* value = EDI_GetElement(segment, 0, 0)) {
        type = QString::fromUtf8(value);
    }

    if (char* value = EDI_GetElement(segment, 1, 0)) {
        if (type == "5") { // port of departure is type = 5
            // TODO #1370 should load port unlocode be filled from a TANSTA message ??
            d->metadata.load_uncode = QString::fromUtf8(value);
            handle_doc_meta(LOAD_UNCODE, d->metadata.load_uncode);
        }
        if (type == "61") { // next call port code is type = 61
            // TODO #1370 are we referring to arrival or departure conditions when talking about port of a TANSTA message ??
            d->metadata.port_uncode = QString::fromUtf8(value);
            handle_doc_meta(PORT_UNCODE, d->metadata.port_uncode);
            // TODO #1370 should discharge port unlocode be filled from a TANSTA message ??
            d->metadata.discharge_uncode = QString::fromUtf8(value);
            handle_doc_meta(DISCHARGE_UNCODE, d->metadata.discharge_uncode);
        }
    }
}


void edifact_parser_t::handleLocTansta(EDI_Segment segment) {
    if (string(EDI_GetCode(segment)) != "LOC") { return; }

    if (char* value = EDI_GetElement(segment, 0, 0)) {
        QString type = QString::fromUtf8(value);
        if (type != "ZZZ") { return; }
    }

    if (char* value = EDI_GetElement(segment, 1, 0)) {
        d->tankCondition.tankCode = QString::fromUtf8(value);
    }
    if (char* value = EDI_GetElement(segment, 1, 3)) {
        d->tankCondition.tankName = QString::fromUtf8(value);
    }
}


void edifact_parser_t::handle_mea(EDI_Segment segment) {
    if (string(EDI_GetCode(segment)) != "MEA") { return; }
    if (EDI_GetElementCount(segment) < 3) { return; }

    if (char* value = EDI_GetElement(segment,0,0)) {
        string type=value;
        if (type=="WT" || type == "VGM" ) {
            if (char* value = EDI_GetElement(segment,2,1)) {
                d->container.weight = lexical_cast<double>(value);
            }
            d->container.weightIsVerified = type == "VGM";
        } else if (type=="AAE") {
            string measure_type(EDI_GetElement(segment,1,0));
            if (measure_type == "G" || measure_type == "AET") {
                if (char* value = EDI_GetElement(segment,2,1)) {
                    const double raw_weight = lexical_cast<double>(value);
                    if (const char* value = EDI_GetElement(segment,2,0)) {
                        QString unit = QString::fromUtf8(value);
                        if (unit == "KGM") {
                            d->container.weight = raw_weight;
                        } else if (unit == "LBR") {
                            d->container.weight = raw_weight * 0.453592;
                        } else {
                            add_warning(QString("Unrecognized or invalid unit for gross weight \"%1\"").arg(unit));;
                        }
                    }
                }
            }
        }
    }
}


void edifact_parser_t::handleMeaTansta(EDI_Segment segment) {
    if (string(EDI_GetCode(segment)) != "MEA") { return; }

    // MEA+WT++TNE:0.0'
    // MEA+DEN++TM3:1.0250'

    QString type;
    if (char* value = EDI_GetElement(segment, 0, 0)) {
        type = QString::fromUtf8(value);
    }

    QString unit;
    if (char* value = EDI_GetElement(segment, 2, 0)) {
        unit = QString::fromUtf8(value);
    }

    double measurement = std::numeric_limits<double>::quiet_NaN();
    if (char* value = EDI_GetElement(segment, 2, 1)) {
        measurement = lexical_cast<double>(value);
    }

    if (type == "WT" && unit == "TNE") {
        d->tankCondition.weight = measurement * ton;
    }

    if (type == "DEN" && unit == "TM3") {
        d->tankCondition.density = measurement * ton/meter3;
    }
}


void edifact_parser_t::handle_nad(EDI_Segment segment) {
    if (string(EDI_GetCode(segment)) != "NAD") { return; }
    if (EDI_GetElementCount(segment) < 2) { return; }

    if (char* value = EDI_GetElement(segment,1,0)) {
        d->container.carrier_code = value;
    }
}

void edifact_parser_t::handle_rff(EDI_Segment segment) {
    if (string(EDI_GetCode(segment)) != "RFF") { return; }
    if (EDI_GetElementCount(segment) < 1) { return; }

    if (char* value = EDI_GetElement(segment,0,0)) {
      string rff_type = value;
      if(rff_type=="BN"){
        d->container.booking_number = EDI_GetElement(segment,0,1);
      } else {
       add_warning(QString("Unsupported RFF type ") + value);
      }
    }
}


void edifact_parser_t::handle_tdt(EDI_Segment segment, bool global) {
    if (string(EDI_GetCode(segment)) != "TDT") { return; }
    if (EDI_GetElementCount( segment ) < 2) { return; }

    QString voyage;
    QString name;
    if (EDI_GetElementCount( segment )>=5) {
        // We could get operator here, too.
        if (char* value = EDI_GetElement(segment,4,3)) {
            name = QString::fromUtf8(value);
        }
    }
    if (char* value = EDI_GetElement(segment,1,0)) {
        voyage = QString::fromUtf8(value);
    }
    if (global) {
        d->metadata.vessel_name = name;
        d->metadata.voyage_code = voyage;
        handle_doc_meta( VESSEL_NAME, d->metadata.vessel_name);
        handle_doc_meta( VOYAGE_CODE, d->metadata.voyage_code);
    }
}

void edifact_parser_t::handle_tdt_baplie(EDI_Segment segment, bool global){
    if (string(EDI_GetCode(segment)) != "TDT") { return; }

    QString voyage_code;
    QString vessel_code;
    QString vessel_name;
    QString carrier_code;
    const bool format15 = d->metadata.message_release == "911";
    //    We use the 95B encoding as default
    int vessel_code_element_no = 7; // vessel code is not very well defined.
    int vessel_code_component_no = 0;
    int vessel_name_element_no = 7;
    int vessel_name_component_no = 3;
    if (format15) {
        vessel_code_element_no=3;
        vessel_code_component_no=0;
        vessel_name_element_no=3;
        vessel_name_component_no=3;
    }
    if (char* value = EDI_GetElement(segment,vessel_code_element_no,vessel_code_component_no)) {
        vessel_code = QString::fromUtf8(value);
    }
    if (char* value = EDI_GetElement(segment,vessel_name_element_no,vessel_name_component_no)) {
        vessel_name = QString::fromUtf8(value);
    }
    if (char* value = EDI_GetElement(segment,4,0)) {
        carrier_code = QString::fromUtf8(value);
    }
    if (char* value = EDI_GetElement(segment,1,0)) {
        voyage_code = QString::fromUtf8(value);
    }
    if (global) {
        d->metadata.vessel_name = vessel_name;
        d->metadata.vessel_code = vessel_code;
        d->metadata.voyage_code = voyage_code;
        d->metadata.carrier_code = carrier_code;
        handle_doc_meta( VESSEL_NAME, d->metadata.vessel_name);
        handle_doc_meta( VESSEL_CODE, d->metadata.vessel_code);
        handle_doc_meta( VOYAGE_CODE, d->metadata.voyage_code);
        handle_doc_meta( CARRIER_CODE, d->metadata.carrier_code);
    }
}


void edifact_parser_t::handle_tdt_movins(EDI_Segment segment, bool global) {
    if (string(EDI_GetCode(segment)) != "TDT") { return; }
    if (EDI_GetElementCount( segment ) < 2) { return; }

    QString voyage_code;
    QString vessel_code;
    QString vessel_name;
    QString carrier_code;
    if (EDI_GetElementCount( segment )>=5) {
        // We could get operator here, too.
        if (char* value = EDI_GetElement(segment,7,0)) {
            vessel_code = QString::fromUtf8(value);
        }
        if (char* value = EDI_GetElement(segment,7,3)) {
            vessel_name = QString::fromUtf8(value);
        }
        if (char* value = EDI_GetElement(segment,4,0)) {
            carrier_code = QString::fromUtf8(value);
        }
    }
    if (char* value = EDI_GetElement(segment,1,0)) {
        voyage_code = QString::fromUtf8(value);
    }
    if (global) {
        d->metadata.vessel_name = vessel_name;
        d->metadata.vessel_code = vessel_code;
        d->metadata.voyage_code = voyage_code;
        d->metadata.carrier_code = carrier_code;
        handle_doc_meta( VESSEL_NAME, d->metadata.vessel_name);
        handle_doc_meta( VESSEL_CODE, d->metadata.vessel_code);
        handle_doc_meta( VOYAGE_CODE, d->metadata.voyage_code);
        handle_doc_meta( CARRIER_CODE, d->metadata.carrier_code);
    }
}


void edifact_parser_t::handleTdtTansta(EDI_Segment segment) {
    if (string(EDI_GetCode(segment)) != "TDT") { return; }

    if (char* value = EDI_GetElement(segment, 1, 0)) {
        d->metadata.voyage_code = QString::fromUtf8(value);
    }
    if (char* value = EDI_GetElement(segment, 4, 0)) {
        d->metadata.carrier_code = QString::fromUtf8(value);
    }
    if (char* value = EDI_GetElement(segment, 7, 0)) {
        d->metadata.vessel_code = QString::fromUtf8(value);
    }
    if (char* value = EDI_GetElement(segment, 7, 3)) {
        d->metadata.vessel_name = QString::fromUtf8(value);
    }
    handle_doc_meta(VESSEL_NAME, d->metadata.vessel_name);
    handle_doc_meta(VESSEL_CODE, d->metadata.vessel_code);
    handle_doc_meta(VOYAGE_CODE, d->metadata.voyage_code);
    handle_doc_meta(CARRIER_CODE, d->metadata.carrier_code);
}


void edifact_parser_t::handle_tmp(EDI_Segment segment){
    if (string(EDI_GetCode(segment)) != "TMP") { return; }

    d->container.reefer = true;
    char* value = EDI_GetElement(segment,0,0);
    if (!value || 2 != lexical_cast<int>(value)) { return; }

    value = EDI_GetElement(segment,1,0);
    if (!value) { return; }

    d->container.reefer_temperature = lexical_cast<double>(value);
    value = EDI_GetElement(segment,1,1);
    if (!value) { return; }

    QString unit = QString::fromUtf8(value);
    if (unit == "CEL") {
    } else if (unit == "FAH") {
        d->container.reefer_temperature = (d->container.reefer_temperature - 32.0)*5.0/9.0; //  FAH -> CEL
    } else {
        add_warning(QString("Unrecognized or invalid unit for reefer temperature \"%1\"").arg(unit));;
    }
}


void edifact_parser_t::handle_segment(EDI_Parameters parameters, EDI_Segment segment, EDI_Directory directory) {
    try {
        switch (d->metadata.doc_type) {
            case BAPLIE:
                handle_baplie_segment(parameters, segment, directory);
                break;
            case COARRI:
                handle_coarri_segment(parameters, segment, directory);
                break;
            case COPRAR:
                handle_coprar_segment(parameters, segment, directory);
                break;
            case MOVINS:
                handle_movins_segment(parameters, segment, directory);
                break;
            case TANSTA:
                handleTanstaSegment(parameters, segment, directory);
                break;
            case UNKNOWN_DOC_TYPE:
                break;
        }
    } catch(std::exception& e) {
        add_warning( e.what() );
    }
}


void edifact_parser_t::handle_start(edi_event_t event, EDI_Parameters parameters) {
    switch (event) {
        case EDI_LOOP: {
            char* p = EDI_GetParameter(parameters, Code);
            assert(p);
            int code = p?lexical_cast<int>(p):-1;
            d->codes.push_back(code);
            if ((d->metadata.doc_type == BAPLIE && code==2) || (d->metadata.doc_type == COARRI && code==3)
                || (d->metadata.doc_type == COPRAR && code==3) || (d->metadata.doc_type == MOVINS && code==3) ) {
                // Start new container
                d->container = edifact_parser_private_t::Container();
            //initialize bay/row/tier
            d->container.bay=0;
            d->container.row=0;
            d->container.tier=0;
            d->container.reefer_temperature = 999.0;
            if (d->metadata.doc_type==COARRI) {
                if (d->metadata.direction == LOAD) {
                    d->container.load_uncode = d->metadata.load_uncode;
                }
            } else if (d->metadata.doc_type==COPRAR) {
                if (d->metadata.direction == LOAD) {
                    d->container.load_uncode = d->metadata.load_uncode;
                }
            }
                }
        }
        break;
        case EDI_TRANSACTION: {
            char* type = EDI_GetParameter(parameters, MessageType);
            char* version = EDI_GetParameter(parameters, MessageVersionNumber);
            char* release = EDI_GetParameter(parameters, MessageReleaseNumber);
            assert(version);
            assert(release);
            assert(type);
            d->metadata.message_version = QString(version);
            d->metadata.message_release = QString(release);
            if (string("BAPLIE")==type) {
                d->metadata.doc_type = BAPLIE;
                handle_doc_type(BAPLIE);
                d->metadata.direction = DISCHARGE;
            } else if (string("COARRI")==type) {
                d->metadata.doc_type = COARRI;
                handle_doc_type(COARRI);
            } else if (string("COPRAR")==type) {
                d->metadata.doc_type = COPRAR;
                handle_doc_type(COPRAR);
            } else if (string("MOVINS")==type) {
                d->metadata.doc_type = MOVINS;
                handle_doc_type(MOVINS);
            } else if (string("TANSTA")==type) {
                d->metadata.doc_type = TANSTA;
                handle_doc_type(TANSTA);
            } else {
                throw std::runtime_error(string("Unsupported messagetype ") + type);
            }
        }
        break;
        default:
        break;
    }
}

void edifact_parser_t::handle_end(edi_event_t event) {
    if (event==EDI_LOOP) { // triggered only if d95b.xml has been read
        if (!d->codes.empty()) {
            if ((d->metadata.doc_type == BAPLIE && d->codes.back() == 2) || (d->metadata.doc_type == COARRI && d->codes.back()==3)
                || (d->metadata.doc_type == COPRAR && d->codes.back() == 3) || (d->metadata.doc_type == MOVINS && d->codes.back() == 3) ) {
                Container container(d->container.equipmentnumber, d->container.weight * kilogram, d->container.isocode);
                container.setWeightIsVerified(d->container.weightIsVerified);
                container.setLive(d->container.reefer ? Container::Live : Container::NonLive);
                container.setTemperature(d->container.reefer_temperature);
                container.setEmpty(d->container.empty);
                container.setDangerousGoodsCodes(d->container.dangerous_goods_codes);
                container.setOog(d->container.oog);
                container.setBookingNumber(d->container.booking_number);
                container.setCarrierCode(d->container.carrier_code);
                container.setLoadPort(d->container.load_uncode);
                container.setDischargePort(d->container.discharge_uncode);
                container.setPlaceOfDelivery(d->container.placeOfDelivery);
                container.setHandlingCodes(d->container.handling_codes);
                handle_container(container);
                handle_container_location(container, ange::vessel::BayRowTier(d->container.bay, d->container.row, d->container.tier));
            } else if ((d->metadata.doc_type == TANSTA && d->codes.back() == 2)) {
                handleTankCondition(d->tankCondition);
            }
        d->codes.pop_back();
        }
    }
}

void edifact_parser_t::handle_container(const ange::containers::Container& /*container*/) {
}

void edifact_parser_t::handle_container_location(const ange::containers::Container& container, const ange::vessel::BayRowTier& position) {
#if NOISY_DEBUG
    QDebug deb = qDebug();  // delaying destruction, and therefore newlines printet until exiting this function
    deb << "CONTAINER: " << container << position << container.load_port() << container.discharge_port();
    deb << container.carrier_code() << container.booking_number();
    deb << (container.empty() ? QString("E") : QString("F")) << (container.raw_powered() ? container.reefer_temperature() : 0.0);
    deb << container.handlingCodes().size();
    for(int i = 0; i < container.handlingCodes().size(); ++i){
        deb << container.handlingCodes().at(i);
    }
    if(container.oog()){
        deb << container.oog()->top();
        deb << container.oog()->front();
        deb << container.oog()->back();
        deb << container.oog()->left();
        deb << container.oog()->right();
    }
#else
Q_UNUSED(container);
Q_UNUSED(position);
#endif
}

void edifact_parser_t::handleTankCondition(const TankCondition& tankCondition) {
#if NOISY_DEBUG
    qDebug() << "TANK:" << tankCondition.tankCode << tankCondition.tankName << tankCondition.weight/ton << tankCondition.density/ton_per_meter3;
#else
    Q_UNUSED(tankCondition);
#endif
}

void edifact_parser_t::handle_doc_type(const ange::edifact::edifact_parser_t::doc_type_t doctype) {
#if NOISY_DEBUG
    switch (doctype) {
        case MOVINS:
            qDebug() << "MOVINS";
            break;
        case BAPLIE:
            qDebug() << "BAPLIE";
            break;
        case COARRI:
            qDebug() << "COARRI";
            break;
        case COPRAR:
            qDebug() << "COPRAR";
            break;
        case TANSTA:
            qDebug() << "TANSTA";
            break;
        case UNKNOWN_DOC_TYPE:
            qDebug() << "UNKNOWN_DOC_TYPE";
            break;
        default:
            break;
    }
#else
Q_UNUSED(doctype);
#endif
}

void edifact_parser_t::handle_doc_meta(edifact_parser_t::meta_data_type_t type, QString data) {
#if NOISY_DEBUG
  switch (type) {
    case VESSEL_NAME:
        qDebug() << "VESSEL_NAME: " << data;
      break;
    case VESSEL_ARRIVAL_TIME:
        qDebug() << "VESSEL_ARRIVAL_TIME: " << data;
      break;
    case VOYAGE_CODE:
        qDebug() << "VOYAGE_CODE: " << data;
      break;
    case VESSEL_CODE:
        qDebug() << "VESSEL_CODE: " << data;
      break;
    case CARRIER_CODE:
        qDebug() << "CARRIER_CODE: " << data;
      break;
    case PORT_UNCODE:
        qDebug() << "PORT_UNCODE: " << data;
        break;
    case LOAD_UNCODE:
        qDebug() << "LOAD_UNCODE: " << data;
        break;
    case DISCHARGE_UNCODE:
        qDebug() << "DISCHARGE_UNCODE: " << data;
        break;
    case MESSAGE_REF:
        qDebug() << "MESSAGE_REF: " << data;
        break;
    case MESSAGE_TIME:
        qDebug() << "MESSAGE_TIME: " << data;
        break;
  }
#else
Q_UNUSED(type);
Q_UNUSED(data);
#endif

}

void edifact_parser_t::handle_section(edifact_parser_t::section_type_t section_type, int section) {
#if NOISY_DEBUG
  switch (section_type) {
    case DIRECTION_SECTION_TYPE: {
      direction_t direction = static_cast<direction_t>(section);
      switch (direction) {
        case LOAD: {
          qDebug() << "LOAD direction";
        }
        break;
        case DISCHARGE: {
          qDebug() << "DISCHARGE direction";
        }
        break;
        case RESTOW: {
          qDebug() << "RESTOW direction";
        }
        break;
        default:
          break;
      }
    }
    break;
    default:
      break;
  }
#else
Q_UNUSED(section_type);
Q_UNUSED(section);
#endif
}


const QStringList& edifact_parser_t::warnings() const {
  return d->warnings;
}

const QString edifact_parser_t::port() const {
  if(!d->metadata.port_uncode.isEmpty()){
    return d->metadata.port_uncode;
  } else {
    return d->metadata.direction==LOAD ? d->metadata.load_uncode : d->metadata.discharge_uncode;
  }
}

const QString& edifact_parser_t::carrierCode() const {
    return d->metadata.carrier_code;
}

const QString& edifact_parser_t::vessel_name() const {
  return d->metadata.vessel_name;
}

const QString& edifact_parser_t::vessel_code() const {
    return d->metadata.vessel_code;
}

const QString& edifact_parser_t::voyage_code() const {
  return d->metadata.voyage_code;
}

}
}
