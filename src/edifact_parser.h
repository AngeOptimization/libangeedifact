#ifndef LIBANGEEDIFACT_EDIFACT_PARSER_H
#define LIBANGEEDIFACT_EDIFACT_PARSER_H
//
// Copyright Ange Optimization Aps, 2008.
// Author Esben Mose Hansen esben@ange.dk
//
#include "medicipp.h"
#include <ange/containers/container.h>
#include "angeedifact_export.h"

#include <vector>
#include <limits>
#include <QSharedDataPointer>
#include <ange/vessel/bayrowtier.h>

namespace ange {
namespace edifact {

struct edifact_parser_private_t;

class ANGEEDIFACT_EXPORT edifact_parser_t : protected medicipp_t {
  private:
    QSharedDataPointer<edifact_parser_private_t> d;
    friend class edifact_parser_private_t;

  public:
    enum doc_type_t{
        UNKNOWN_DOC_TYPE,
        BAPLIE,
        COPRAR,
        MOVINS,
        COARRI,
        TANSTA
    };

  protected:
    enum direction_t {
      UNKNOWN_DIRECTION,
      LOAD,
      DISCHARGE,
      RESTOW
    };

    enum section_type_t {
      DIRECTION_SECTION_TYPE
    };

    enum meta_data_type_t {
      VOYAGE_CODE,
      VESSEL_NAME,
      VESSEL_CODE,
      CARRIER_CODE,
      LOAD_UNCODE,
      DISCHARGE_UNCODE,
      MESSAGE_TIME,
      MESSAGE_REF,
      VESSEL_ARRIVAL_TIME,
      PORT_UNCODE
    };
  public:
    /**
     * Uses built in resource ":/libangeedifact/data/d95b_sloppy_d93a_tansta.xml" for configuration of medici edifact C lib
    */
    edifact_parser_t();

    virtual ~edifact_parser_t();
    void parse(QIODevice* input);

    const QStringList& warnings() const ;

    /**
     * @return carrier code for document (after parse)
     * See also vessel_code
     */
    const QString& carrierCode() const;

    /**
     * @return vessel name for document (after parse)
     * See also voyage_code and port
     */
    const QString& vessel_name() const;

    /**
     * @return vessel code for document (after parse)
     * See also vessel_name
     */
    const QString& vessel_code() const;

    /**
     * @return voyage code for document (after parse)
     * See also port and vessel_name
     */
    const QString& voyage_code() const;

    /**
     * @return uncode/port for document (empty if none)
     * See also voyage_code and vessel_name
     */
    const QString port() const;


    /**
     * Class to transport tank conditions, as vessel library does not have any suitable
     */
    struct TankCondition {
        TankCondition() : tankCode(""), tankName(""),
            weight(std::numeric_limits<double>::quiet_NaN() * ange::units::ton),
            density(std::numeric_limits<double>::quiet_NaN() * ange::units::ton/ange::units::meter3) {};
        QString tankCode;
        QString tankName;
        ange::units::Mass weight;
        ange::units::Density density;
    };


  private:
    // Handles for events from medici
    void handle_segment(EDI_Parameters parameters, EDI_Segment segment, EDI_Directory directory);
    void handle_start(edi_event_t event, EDI_Parameters parameters);
    void handle_end(edi_event_t event);

    // parsing specific segments in edifact files
    void handle_baplie_segment(EDI_Parameters parameters, EDI_Segment segment, EDI_Directory directory);
    void handle_coarri_segment(EDI_Parameters parameters, EDI_Segment segment, EDI_Directory directory);
    void handle_coprar_segment(EDI_Parameters parameters, EDI_Segment segment, EDI_Directory directory);
    void handle_movins_segment(EDI_Parameters parameters, EDI_Segment segment, EDI_Directory directory);
    void handleTanstaSegment(EDI_Parameters parameters, EDI_Segment segment, EDI_Directory directory);

    void handle_dgs(EDI_Segment segment);
    void handle_dim(EDI_Segment segment);
    void handle_dtm(EDI_Segment segment, bool global, bool vessel);
    void handle_eqd(EDI_Segment segment);
    void handle_ftx_han(EDI_Segment segment);
    void handle_han_direction(EDI_Segment segment);
    void handle_loc_container_coprar(EDI_Segment segment);
    void handle_loc_container_baplie_coarri(EDI_Segment segment);
    void handle_loc_container_movins(EDI_Segment segment);
    void handle_loc_metadata(EDI_Segment segment);
    void handle_loc_metadata_coprar(EDI_Segment segment);
    void handleLocMetadataTansta(EDI_Segment segment);
    void handleLocTansta(EDI_Segment segment);
    void handle_mea(EDI_Segment segment);
    void handleMeaTansta(EDI_Segment segment);
    void handle_nad(EDI_Segment segment);
    void handle_tdt(EDI_Segment segment, bool global);
    void handle_tdt_baplie(EDI_Segment segment, bool global);
    void handle_tdt_movins(EDI_Segment segment, bool global);
    void handleTdtTansta(EDI_Segment segment);
    void handle_tmp(EDI_Segment segment);
    void handle_rff(EDI_Segment segment);

    void add_warning(const QString& text);

  protected:

    // BAPLIE, COARRI, COPRAR, MOVINS
    virtual void handle_container(const ange::containers::Container& container);
    virtual void handle_container_location(const ange::containers::Container& container, const ange::vessel::BayRowTier& position);

    // TANSTA
    virtual void handleTankCondition(const TankCondition& tankCondition);

    // general EDIFACT
    virtual void handle_section(section_type_t section_type, int section);
    virtual void handle_doc_type(const doc_type_t doctype);
    virtual void handle_doc_meta(meta_data_type_t type, QString data);
    void handle_warning(int error);
    void handle_error(int error);
};

}
}
#endif
