#ifndef LIBANGEEDIFACT_MEDICIPP_H
#define LIBANGEEDIFACT_MEDICIPP_H
//
// Copyright Ange Optimization Aps, 2008.
// Author Esben Mose Hansen esben@ange.dk
//

#include <medici.h>
#include <string>
#include "angeedifact_export.h"

class QIODevice;

namespace ange {
namespace edifact {

class ANGEEDIFACT_EXPORT medicipp_t  {
  private:
    EDI_Directory m_directory;
    EDI_Parser m_parser;
  public:
    medicipp_t(const std::string& xmltsgfile);
    medicipp_t(char* buffer, unsigned int size);
    virtual ~medicipp_t();

    void parse(QIODevice* stream);
    EDI_Parser parser() { return m_parser; }
    EDI_Directory directory() { return m_directory; }

    const char* error_string(int error);
    long current_segment_index();
    long current_byte_index();

    virtual void handle_warning(int error) = 0;
    virtual void handle_error(int error) = 0;
    virtual void handle_segment(EDI_Parameters parameters, EDI_Segment segment, EDI_Directory directory) = 0;
    virtual void handle_start(edi_event_t event, EDI_Parameters parameters) = 0;
    virtual void handle_end(edi_event_t event) = 0;
  private:
    void initialize_parser();
};

}
}
#endif